
# Grupo 03

## Introdução

Adicionar introdução...

## Configuração

### Instalação

Execute o comando:
```shell
pip install mkdocs mkdocs-material
```

### Executar localmente

Para servir os arquivos estáticos:
```shell
mkdocs serve
```

## Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`